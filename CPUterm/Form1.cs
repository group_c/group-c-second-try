﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenHardwareMonitor.Hardware;
using OpenHardwareMonitor;
using System.Management;

namespace CPUterm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            notifyIcon1.Visible = false;
            this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(notifyIcon1_MouseDoubleClick);
            this.Resize += new System.EventHandler(this.Form1_Resize);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = "";
            timer1.Stop();
            timer1.Interval = 1000;
            timer1.Tick+=new EventHandler(temp);
            timer1.Start();
        }
        public void temp(object sender, EventArgs e)
        {
            Temp t = new Temp();
            label1.Text = t.temp;
            notifyIcon1.Text = t.temp;
            double icon = t.t;
            if (icon < 35) { notifyIcon1.Icon = new Icon("zelenyy-krug.ico"); }
            if ((icon >= 35) && (icon < 60)) { notifyIcon1.Icon = new Icon("oranzhevyy-krug.ico"); }
            if (icon >= 60) { notifyIcon1.Icon = new Icon("krasnyy-krug.ico"); }
            if (icon >= 90) MessageBox.Show("МЫ ГОРИМ!!!", "Windows с Вами прощается:)");
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            notifyIcon1.Visible = false;
            this.ShowInTaskbar = true;
            WindowState = FormWindowState.Normal;
        }
    }
    public class Temp
    {
        public string temp = "";
        public double t = 0;
        public Temp()
        {
            Computer computer = new Computer();
            computer.Open();
            computer.CPUEnabled = true;
            foreach (var hardwareItem in computer.Hardware)
            {
                if (hardwareItem.HardwareType == HardwareType.CPU)
                {
                    hardwareItem.Update();
                    foreach (IHardware subHardware in hardwareItem.SubHardware)
                        subHardware.Update();
                    foreach (var sensor in hardwareItem.Sensors)
                    {
                        if (sensor.SensorType == SensorType.Temperature)
                        {
                            t = t + sensor.Value.Value;
                            temp += String.Format("{0} = {1}°\r\n", sensor.Name, sensor.Value.HasValue ? sensor.Value.Value.ToString() : "no value");
                        }
                    }
                }
            }
            t = Math.Round(t / 3, 2);
        }
    }
}
