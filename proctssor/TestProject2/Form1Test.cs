﻿using proctssor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using System.Management;
using System.Management.Instrumentation;
using OpenHardwareMonitor.Hardware;

namespace TestProject2
{
    
    
    /// <summary>
    ///Это класс теста для Form1Test, в котором должны
    ///находиться все модульные тесты Form1Test
    ///</summary>
    [TestClass()]
    public class Form1Test
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///Тест для backgroundWorker1_DoWork
        ///</summary>
        [TestMethod()]
        [DeploymentItem("proctssor.exe")]
        public void backgroundWorker1_DoWorkTest()
        {
            bool test = false;
            OpenHardwareMonitor.Hardware.Computer comp =
                   new OpenHardwareMonitor.Hardware.Computer() { CPUEnabled = true};
                    comp.Open();
                    foreach (var hardwareItem in comp.Hardware)
                    {
                        if (hardwareItem.HardwareType == HardwareType.CPU)
                        {
                            hardwareItem.Update();
                            foreach (IHardware subHardware in hardwareItem.SubHardware)
                                subHardware.Update();

                            foreach (var sensor in hardwareItem.Sensors)
                            {
                                if (sensor.SensorType == SensorType.Temperature)
                                {
                                    if (sensor.Value != null)
                                    {
                                        test = true;
                                    }
                                }
                            }
                        }
                    }
                    Assert.AreEqual(true, test);
        }

        /// <summary>
        ///Тест для backgroundWorker1_DoWork
        ///</summary>
        [TestMethod()]
        [DeploymentItem("proctssor.exe")]
        public void backgroundWorker1_DoWorkTest1()
        {
            bool test = false;
            OpenHardwareMonitor.Hardware.Computer comp =
                   new OpenHardwareMonitor.Hardware.Computer() { MainboardEnabled = true };
            comp.Open();
            foreach (var hardwareItem in comp.Hardware)
            {
                if (hardwareItem.HardwareType == HardwareType.Mainboard)
                {
                    hardwareItem.Update();
                    foreach (IHardware subHardware in hardwareItem.SubHardware)
                        subHardware.Update();
                    if (hardwareItem.SubHardware.Length == 1)
                        foreach (var sensor in hardwareItem.SubHardware[0].Sensors)
                        {
                            if (sensor.SensorType == SensorType.Temperature && sensor.Name == "CPU")
                            {
                                if (sensor != null)
                                { test = true; }
                            }
                        }
                }
            }
            Assert.AreEqual(true, test);
        }
    }
}
