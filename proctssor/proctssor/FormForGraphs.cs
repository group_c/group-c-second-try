﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace proctssor
{
    public partial class FormForGraphs : Form
    {
        GraphicGroup g;
        public FormForGraphs(GraphicGroup _g)
        {
            InitializeComponent();
            this.g = _g;
            DrawGraphic(1);
        }
        public void DrawGraphic(int mashtab)
        {
            Dictionary<string, Array> d = new Dictionary<string, Array>();
            this.Text = g.Title;// Название формы ставим.
            int h = 0;
            int m = 0;
            string DataForParsing = g.Title;
            int i = 0;
            while (true) //  Распарсим название , извлечем из неё исходные часы и минуты - я не нашел этих данных в БД в другом месте.
            {
                if (DataForParsing[i].CompareTo(':') == 0)
                {
                    h = Convert.ToInt32(DataForParsing[i - 2]) * 10 + Convert.ToInt32(DataForParsing[i - 1]);
                    m = Convert.ToInt32(DataForParsing[i + 1]) * 10 + Convert.ToInt32(DataForParsing[i + 2]);
                    break;
                }
                i++;
            }


            i = 0;
            int formashtab = 1;
            using (var db = new GraphicContext())
            {
                var Graphics = from b in db.Graphics
                             orderby b.Id
                             where b.GraphGroupId == g.Id
                             select b;

            foreach (Graphic grafik in Graphics) // В этом цикле заносим все точки в переменную - массив (почти как в основном окне)
            {
                try
                {
                    chart1.Series[i].Name = grafik.Title; // Название графика ставим
                    List<double> mas = new List<double>();
                    var Dots = from b in db.Dots
                                 orderby b.Id
                                 where b.GraphId == grafik.Id
                                 select b
                                 ;
                    foreach (Dot dot in Dots)  // Отрисовываю по одному цельному графику для каждого ядра.   
                    {
                        if (formashtab == mashtab) // Исправить! Добавить масштабы!
                        {
                            mas.Add(dot.YGrade);
                            string datePatt = @"HH:mm:ss";
                            chart1.Series[i].Points.AddXY(new DateTime(dot.XTime).ToString(datePatt), Math.Round(dot.YGrade, 2));
                            chart1.Series[i].XValueMember = new DateTime(dot.XTime).ToString(datePatt);
                            chart1.Series[i].YValueMembers = Math.Round(dot.YGrade, 2).ToString();
                            formashtab = 1;
                        }
                        else
                        {
                            formashtab++;
                        }
                    }
                    /*d.Add(h.ToString() + ":" + m.ToString(), mas.ToArray<double>());
                    foreach ( double temperatura in mas)
                    {
                        chart1.Series[i].Points.AddXY(h+":"+m,temperatura);
                        chart1.Series[i].XValueMember = h + ":" + m;
                        chart1.Series[i].YValueMembers = temperatura.ToString();
                    }*/
                    i++;
                }
                catch
                {
                    break;
                }
                m++;
                if (m == 60)
                {
                    m = 0;
                    h++;
                }
            }
            }


            foreach (var graf in chart1.Series)
            {
                if (graf.Points.Count == 0) graf.IsVisibleInLegend = false;
            }
            chart1.Invalidate();
        }

        private void FormForGraphs_Load(object sender, EventArgs e)
        {

        }
    }
}
