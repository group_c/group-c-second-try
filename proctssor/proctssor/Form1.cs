﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Management;
using System.Management.Instrumentation;
using OpenHardwareMonitor.Hardware;
using System.Threading;
using Microsoft.Win32;

namespace proctssor
{
    public partial class Form1 : Form
    {
        GraphicContext db;// переменная контекста данных
        DateTime statisBegin;// время начала отсчета
        long timeTicks;// время в тактах следующего сохраняемого замера
        int newIdGraphGroup;// id группы графиков 
        int[] newIdGraph;// id  графиков
        ulong meanSum = 0;// сумма средних значений за время сбора данных
        ulong meanCount = 0;// количество средних значений, которые были суммированы за время сбора данных
        bool fistgraph = true;
        int step = 15; //переменная для регулировки частоты сбора данных
        int kolvo = 0;//переменная для хранения количества показателе температуры

        //перемены для работы с индикаторами
        string zelenyy = "ico\\zelenyy-krug.ico";
        string oranzhevyyIcon = "ico\\oranzhevyy-krug.ico";
        string krasnyyIcon = "ico\\krasnyy-krug.ico";
        Bitmap myImage;
        Icon myIcon;

        const string name = "Температура CPU";

        public Form1()
        {
            InitializeComponent();
            SetAutorunValue(true);
            notifyIcon1.Text = "Измерение температуры процессора";
            notifyIcon1.ShowBalloonTip(5000, "Приложение запущено", "Чтобы развернуть окно кликните значок приложения дважды", ToolTipIcon.Info);
            this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(notifyIcon1_MouseDoubleClick);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            timer1.Interval = 1000;
            this.ShowInTaskbar = false;
            this.Hide();
            db = new GraphicContext();
            backgroundWorker1.RunWorkerAsync();
            label1.Text = "";
        }

        int timeOut = 1800;//переменная для регулировки частоты вывода сообщения о перегреве
        Dictionary<string, Array> d = new Dictionary<string, Array>();
        List<string> names = new List<string>();
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                contextMenuStrip1.Items[1].Text = "Развернуть";
            }
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
            WindowState = FormWindowState.Normal;
            contextMenuStrip1.Items[1].Text = "Свернуть";
        }
        private void развернитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contextMenuStrip1.Items[1].Text == "Развернуть")
            {
                this.Show();
                this.ShowInTaskbar = true;
                WindowState = FormWindowState.Normal;
                contextMenuStrip1.Items[1].Text = "Свернуть";
            }
            else
            {
                this.Hide();
                this.ShowInTaskbar = false;
                contextMenuStrip1.Items[1].Text = "Развернуть";
            }
        }
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void начатьСборСтатистикиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contextMenuStrip1.Items[0].Text == "Начать сбор статистики")
            {
                this.Width = 985;
                for (int i = 0; i < kolvo; i++)
                {
                    chart1.Series[i].Points.Clear();
                }
                timer1.Tick += new EventHandler(Sbor);
                workSbor(sender, e);
                contextMenuStrip1.Items[0].Text = "Остановить сбор статистики";
                button1.Text = "Остановить сбор статистики";
                d.Clear();
            }
            else
            {
                this.Width = 282;
                this.Show();
                this.ShowInTaskbar = true;
                WindowState = FormWindowState.Normal;
                timer1.Tick -= new EventHandler(Sbor);
                Postr(1);
                workSbor(sender, e);
                contextMenuStrip1.Items[0].Text = "Начать сбор статистики";
                button1.Text = "Начать сбор статистики";
            }
        }
        private int addGroupGraphics(string Title = "", double meansTotal = 0)
        {
            using (var db = new GraphicContext())
            {
                var graphicGroup = new GraphicGroup { Title = Title, meansTotal = meansTotal };
                db.GraphicGroups.Add(graphicGroup);
                db.SaveChanges();
               return graphicGroup.Id;
            }
        }
        public string removeGroupGraphics(int id = -1)
        {
            using (var db = new GraphicContext())
            {
                if (id > -1)
                {
                    var graphicGroup = db.GraphicGroups.Find(id);
                    if (graphicGroup != null)
                    {
                        db.GraphicGroups.Remove(graphicGroup);
                        db.SaveChanges();
                        return "Группа графиков удалена";
                    }
                }
                    return "Группы графиков с таким Id нет";
            }
        }
        public string editGroupGraphics(int id = -1, string Title = "", double meansTotal = 0)
        {
            if (id > -1)
            {
                using (var db = new GraphicContext())
                {
                    var graphicGroup = db.GraphicGroups.Find(id);
                    if (graphicGroup != null)
                    {
                        if(Title != "no_cheng")
                            graphicGroup.Title = Title;
                        graphicGroup.meansTotal = meansTotal;
                        db.Entry(graphicGroup).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return "Информация по группе графиков изменена";
                    }
                }
            }
            return "Группы графиков с таким Id нет";

        }
        private int addGraphics(string Title = "", int IdGraphGroup = 0)
        {
            //создание  графиков с датчиков процессора
            using (var db = new GraphicContext())
            {
                var graphic = new Graphic { Title = Title, GraphGroupId = IdGraphGroup };
                db.Graphics.Add(graphic);
                db.SaveChanges();
                return graphic.Id;
            }
        }
        public string removeGraphic(int id = -1)
        {
            if (id > -1)
            {
                using (var db = new GraphicContext())
                {
                    var graphic = db.Graphics.Find(id);
                    if (graphic != null)
                    {
                        db.Graphics.Remove(graphic);
                        db.SaveChanges();
                        return "График удален";
                    }
                }
            }
            return "Графика с таким Id нет";
        }
        private int addDots(long  XTime = 0, double YGrade = 0, int GraphId = 0 )
        {
            using (var db = new GraphicContext())
            {
                var dot = new Dot { XTime = XTime, YGrade = YGrade, GraphId = GraphId };
                db.Dots.Add(dot);
                db.SaveChanges();
                return dot.Id;
            }                     
        }
        public string removeDot(int id = -1)
        {
            if (id > -1)
            {
                using (var db = new GraphicContext())
                {
                    var dot = db.Dots.Find(id);
                    if (dot != null)
                    {
                        db.Dots.Remove(dot);
                        db.SaveChanges();
                        return "Точка удалена";
                    }
                }
            }
            return "Точки с таким Id нет";
        }
        private void showAll()
        {
            using (var db = new GraphicContext())
            {
                var query1 = from b in db.Dots
                             orderby b.Id
                             select b;

                foreach (var item in query1)
                {
                    int d = item.Id;
                }
                var query2 = from b in db.Graphics
                             orderby b.Id
                             select b;

                foreach (var item in query2)
                {
                    int g = item.Id;
                }
                var query3 = from b in db.GraphicGroups
                             orderby b.Id
                             select b;

                foreach (var item in query3)
                {
                    int gg = item.Id;
                }
            }
        }
        private void removeAll()
        {
            using (var db = new GraphicContext())
            {
                var query1 = from b in db.Dots
                             orderby b.Id
                             select b;

                foreach (var item in query1)
                {
                    removeDot(item.Id);
                }
                var query2 = from b in db.Graphics
                             orderby b.Id
                             select b;

                foreach (var item in query2)
                {
                    removeGraphic(item.Id);
                }
                var query3 = from b in db.GraphicGroups
                             orderby b.Id
                             select b;

                foreach (var item in query3)
                {
                    removeGroupGraphics(item.Id);
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            workSbor(sender, e);
        }
        private void workSbor(object sender, EventArgs e)
        {
            string datePatt = @"M.d.yyyy HH:mm:ss";
            if (contextMenuStrip1.Items[0].Text == "Начать сбор статистики")
            {
                label1.Text = "Производится сбор информации...";
                MessageBox.Show("Производится сбор информации...");
                fistgraph = true;
                statisBegin = DateTime.Now;
                timeTicks = statisBegin.Ticks;
                newIdGraph = new int[kolvo];
                //создание группы графиков
                newIdGraphGroup = addGroupGraphics(" от " + statisBegin.ToString(datePatt) + " по настоящее время");
                this.Width = 985;
                for (int i = 0; i < kolvo; i++)
                {
                    chart1.Series[i].Points.Clear();
                }
                timer1.Tick += new EventHandler(Sbor);
                contextMenuStrip1.Items[0].Text = "Остановить сбор статистики";
                button1.Text = "Остановить сбор статистики";
                d.Clear();
            }
            else
            {
                label1.Text = "";
                fistgraph = true;
                //изменение названия и среднего значения группы графиков
                if (meanCount < 2)
                {

                    var graphicGroup = db.GraphicGroups.Find(newIdGraphGroup);
                    foreach (Graphic graph in graphicGroup.Graphics.ToList())
                    {
                        foreach(Dot dot in graph.Dots.ToList()){
                            removeDot(dot.Id);
                        }
                        removeGraphic(graph.Id);
                    }
                    removeGroupGraphics(newIdGraphGroup);
                }
                else
                {
                    timeTicks -= 150000000;
                    string Title = " от " + statisBegin.ToString(datePatt) + " до " + new DateTime(timeTicks).ToString(datePatt);
                    editGroupGraphics(newIdGraphGroup, Title, meanSum / meanCount);
                }
                this.Width = 292;
                timer1.Tick -= new EventHandler(Sbor);
                //Postr(1);
                contextMenuStrip1.Items[0].Text = "Начать сбор статистики";
                button1.Text = "Начать сбор статистики";
            }
        }
        public void Sbor(object sender, EventArgs e) // Собираем информацию 
        {
            if (step > 14)
            {
               double[] mas = new double[kolvo];
                int i = 0;
                int sum = 0;
                OpenHardwareMonitor.Hardware.Computer comp = new OpenHardwareMonitor.Hardware.Computer() { CPUEnabled = true, MainboardEnabled = true };
                comp.Open();
                foreach (var hardwareItem in comp.Hardware)
                {
                    if (hardwareItem.HardwareType == HardwareType.CPU)
                    {
                        hardwareItem.Update();
                        foreach (IHardware subHardware in hardwareItem.SubHardware)
                            subHardware.Update();
                        foreach (var sensor in hardwareItem.Sensors)
                        {
                            if (sensor.SensorType == SensorType.Temperature)
                            {
                                mas[i] = Math.Round(sensor.Value.Value, 2);
                                names.Add(sensor.Name);
                                if (fistgraph)
                                {
                                    //создание  графиков с датчиков процессора
                                        newIdGraph[i] = addGraphics(sensor.Name, newIdGraphGroup);
                                    chart1.Series[i].Name = sensor.Name;
                                    chart1.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

                                }
                                sum += (int)Math.Round(sensor.Value.Value, 2);
                                //создание точек  графиков
                                addDots(timeTicks, sensor.Value.Value, newIdGraph[i]);
                                chart1.Series[i].Points.AddXY(DateTime.Now.ToLongTimeString(), Math.Round(sensor.Value.Value, 2));
                                chart1.Series[i].XValueMember = DateTime.Now.ToLongTimeString();
                                chart1.Series[i].YValueMembers = Math.Round(sensor.Value.Value, 2).ToString();
                                i++;
                            }
                        }
                    }
                    else if (hardwareItem.HardwareType == HardwareType.Mainboard)
                    {
                        hardwareItem.Update();
                        foreach (IHardware subHardware in hardwareItem.SubHardware)
                            subHardware.Update();
                        if (hardwareItem.SubHardware.Length == 1)
                            foreach (var sensor in hardwareItem.SubHardware[0].Sensors)
                            {
                                if (sensor.SensorType == SensorType.Temperature && sensor.Name == "CPU")
                                {
                                    mas[i] = Math.Round(sensor.Value.Value, 2);
                                    names.Add(sensor.Name);
                                    if (fistgraph)
                                    {
                                        //создание  графиков с мат. плат
                                        newIdGraph[i] = addGraphics(sensor.Name, newIdGraphGroup);
                                         chart1.Series[i].Name = sensor.Name;
                                         chart1.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                                    }
                                    sum += (int)Math.Round(sensor.Value.Value, 2);
                                    addDots(timeTicks, sensor.Value.Value, newIdGraph[i]);
                                    chart1.Series[i].Points.AddXY(DateTime.Now.ToLongTimeString(), Math.Round(sensor.Value.Value, 2));
                                    chart1.Series[i].XValueMember = DateTime.Now.ToLongTimeString();
                                        chart1.Series[i].YValueMembers = Math.Round(sensor.Value.Value, 2).ToString();
                                    i++;
                                }
                            }
                    }
                }
                foreach (var graf in chart1.Series)
                {
                    if (graf.Points.Count == 0) graf.IsVisibleInLegend = false;
                }
                chart1.Invalidate();
                fistgraph = false;
                try
                {
                    d.Add(DateTime.Now.ToLongTimeString(), mas);
                }
                catch { }
                meanCount++;
                meanSum += (ulong)(sum/i);
                editGroupGraphics(newIdGraphGroup, "no_cheng", meanSum / meanCount);
                step = 0;
                timeTicks += 150000000;
            }
            else
            { 
                step++; 
            }
        }
        public void Postr(int Mashtab) // Выгружаем информацию на график
        {
            for (int i = 0; i < kolvo; i++)
            {
                chart1.Series[i].Points.Clear();
            }
            for (int i = 0; i < kolvo; i++)
            {
                int formashtab = 1;
                chart1.Series[i].Name = names[i]; // Не понимаю, почему тут имен не по количеству ядер, а количество ядер * количество показаний.
                //chart1.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

                foreach (KeyValuePair<string, Array> pair in d) // Просто пропускаем некоторые пары в соответствие с масштабом. 
                {
                    if (formashtab == Mashtab)
                    {
                        chart1.Series[i].Points.AddXY(pair.Key, pair.Value.GetValue(i));
                        chart1.Series[i].XValueMember = pair.Key;
                        chart1.Series[i].YValueMembers = pair.Value.GetValue(i).ToString();
                        formashtab = 1;
                    }
                    else
                    {
                        formashtab++;
                    }

                }
            }
            foreach (var i in chart1.Series)
            {
                if (i.Points.Count == 0) i.IsVisibleInLegend = false;
            }
            chart1.Invalidate(); // Может и не нужна строка... Не знаю точно.
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            foreach (Form f in Application.OpenForms)
            {
                if (f.Name == "Form2")
                {
                    f2.Activate();
                    return;
                }
            }
            f2.ShowDialog();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundWorker1.CancelAsync();
            if (contextMenuStrip1.Items[0].Text == "Остановить сбор статистики")
            {
                //изменение названия и среднего значения группы графиков
                if (meanCount < 2)
                {

                    var graphicGroup = db.GraphicGroups.Find(newIdGraphGroup);
                    foreach (Graphic graph in graphicGroup.Graphics.ToList())
                    {
                        foreach (Dot dot in graph.Dots.ToList())
                        {
                            removeDot(dot.Id);
                        }
                        removeGraphic(graph.Id);
                    }
                    removeGroupGraphics(newIdGraphGroup);
                }
                else
                {
                    string datePatt = @"M.d.yyyy HH:mm:ss";
                    timeTicks -= 150000000;
                    string Title = " от " + statisBegin.ToString(datePatt) + " до " + new DateTime(timeTicks).ToString(datePatt);
                    editGroupGraphics(newIdGraphGroup, Title, meanSum / meanCount);
                }
                this.Width = 282;
                timer1.Tick -= new EventHandler(Sbor);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            while (i == 0)
            {
                double sum = 0;// сумма показателей датчиков
                int count = 0;// подсчет количества датчиков
                string outText = "";// строка в которой формируется выводимая информация
                try
                {
                    OpenHardwareMonitor.Hardware.Computer comp =
                   new OpenHardwareMonitor.Hardware.Computer() { CPUEnabled = true, MainboardEnabled = true };
                    comp.Open();
                    foreach (var hardwareItem in comp.Hardware)
                    {
                        if (hardwareItem.HardwareType == HardwareType.CPU)
                        {
                            hardwareItem.Update();
                            foreach (IHardware subHardware in hardwareItem.SubHardware)
                                subHardware.Update();

                            foreach (var sensor in hardwareItem.Sensors)
                            {
                                if (sensor.SensorType == SensorType.Temperature)
                                {
                                    if (sensor.Value != null)
                                    {
                                        outText += sensor.Name + " : " + Math.Round(sensor.Value.Value, 2) + "°C" + Environment.NewLine;
                                        sum += Math.Round(sensor.Value.Value, 2);
                                        count++;
                                    }
                                }
                            }
                        }
                        else if (hardwareItem.HardwareType == HardwareType.Mainboard)
                        {
                            hardwareItem.Update();
                            foreach (IHardware subHardware in hardwareItem.SubHardware)
                                subHardware.Update();
                            if (hardwareItem.SubHardware.Length == 1)
                                foreach (var sensor in hardwareItem.SubHardware[0].Sensors)
                                {
                                    if (sensor.SensorType == SensorType.Temperature && sensor.Name == "CPU")
                                    {
                                        outText += sensor.Name + " : " + Math.Round(sensor.Value.Value, 2) + "°C" + Environment.NewLine;
                                        sum += Math.Round(sensor.Value.Value, 2);
                                        count++;
                                    }
                                }
                        }
                    }
                    kolvo = count;
                }
                catch (ManagementException ex)
                {
                    MessageBox.Show("Ошибка получения данных " + ex.Message);
                }
                if (sum > 0 && count > 0)
                {
                    double mean = Math.Round(sum / count, 2);
                    outText += "Среднее значение : " + mean + " °C" + Environment.NewLine;
                    backgroundWorker1.ReportProgress(0, outText);
                    Thread.Sleep(5000);
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string[] s = e.UserState.ToString().Split(' ');
            double mean = Convert.ToDouble(s[s.Count() - 2]);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.Icon = myIcon;
            if (mean <= 55)
            {
                myImage = new Bitmap(zelenyy);
                myIcon = new Icon(zelenyy, 50, 50);
            }
            else if (mean > 55 && mean <= 75)
            {
                myImage = new Bitmap(oranzhevyyIcon);
                myIcon = new Icon(oranzhevyyIcon, 50, 50);
            }
            else if (mean > 75)
            {
                myImage = new Bitmap(krasnyyIcon);
                myIcon = new Icon(krasnyyIcon, 50, 50);
            }
            textBox1.Text = e.UserState.ToString();
            pictureBox1.ClientSize = new Size(50, 50);

            Bitmap bitmap = new Bitmap(50, 50);

            System.Drawing.Font drawFont = new System.Drawing.Font("Calibri", 26, FontStyle.Bold);
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);

            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bitmap);
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel;
            graphics.DrawIcon(myIcon, 0, 0);
            graphics.DrawString("" + (int)mean, drawFont, drawBrush, 1, 2);

            //To Save icon to disk
            bitmap.Save("icon.ico", System.Drawing.Imaging.ImageFormat.Icon);

            Icon createdIcon = Icon.FromHandle(bitmap.GetHicon());

            drawFont.Dispose();
            drawBrush.Dispose();
            graphics.Dispose();
            bitmap.Dispose();
            pictureBox1.Image = createdIcon.ToBitmap();
            this.Icon = createdIcon;
            notifyIcon1.Icon = createdIcon;
            notifyIcon1.Text = "Температуры CPU: " + mean + "°C";

            if (mean > 100)
            {
                if (timeOut >= 1800)
                {
                    timeOut = 0;
                    MessageBox.Show("Температура процессора больше 100°C", "Горим!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (timeOut < 1800)
            {
                timeOut++;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public bool SetAutorunValue(bool autorun)
        {
            string ExePath = System.Windows.Forms.Application.ExecutablePath;
            RegistryKey reg;
            reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            try
            {
                if (autorun)
                    reg.SetValue(name, ExePath);
                else
                    reg.DeleteValue(name);

                reg.Close();
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
