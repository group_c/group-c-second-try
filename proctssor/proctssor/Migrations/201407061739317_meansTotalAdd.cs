namespace proctssor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class meansTotalAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Graphics", "meansTotal", c => c.Double(nullable: false));
            AlterColumn("dbo.Dots", "XTime", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Dots", "XTime", c => c.Double(nullable: false));
            DropColumn("dbo.Graphics", "meansTotal");
        }
    }
}
