namespace proctssor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        XTime = c.Double(nullable: false),
                        YGrade = c.Double(nullable: false),
                        GraphId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Graphics", t => t.GraphId)
                .Index(t => t.GraphId);
            
            CreateTable(
                "dbo.Graphics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dots", "GraphId", "dbo.Graphics");
            DropIndex("dbo.Dots", new[] { "GraphId" });
            DropTable("dbo.Graphics");
            DropTable("dbo.Dots");
        }
    }
}
