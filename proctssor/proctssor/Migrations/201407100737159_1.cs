namespace proctssor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups");
            DropIndex("dbo.Graphics", new[] { "GraphGroupId" });
            AlterColumn("dbo.Graphics", "GraphGroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.Graphics", "GraphGroupId");
            AddForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups");
            DropIndex("dbo.Graphics", new[] { "GraphGroupId" });
            AlterColumn("dbo.Graphics", "GraphGroupId", c => c.Int());
            CreateIndex("dbo.Graphics", "GraphGroupId");
            AddForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups", "Id");
        }
    }
}
