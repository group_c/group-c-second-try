namespace proctssor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dots", "GraphId", "dbo.Graphics");
            AddColumn("dbo.Dots", "Graphic_Id", c => c.Int());
            AddColumn("dbo.Dots", "Graphic_Id1", c => c.Int());
            CreateIndex("dbo.Dots", "Graphic_Id");
            CreateIndex("dbo.Dots", "Graphic_Id1");
            AddForeignKey("dbo.Dots", "Graphic_Id1", "dbo.Graphics", "Id");
            AddForeignKey("dbo.Dots", "Graphic_Id", "dbo.Graphics", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dots", "Graphic_Id", "dbo.Graphics");
            DropForeignKey("dbo.Dots", "Graphic_Id1", "dbo.Graphics");
            DropIndex("dbo.Dots", new[] { "Graphic_Id1" });
            DropIndex("dbo.Dots", new[] { "Graphic_Id" });
            DropColumn("dbo.Dots", "Graphic_Id1");
            DropColumn("dbo.Dots", "Graphic_Id");
            AddForeignKey("dbo.Dots", "GraphId", "dbo.Graphics", "Id");
        }
    }
}
