namespace proctssor.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGraphicGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GraphicGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        meansTotal = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Graphics", "GraphGroupId", c => c.Int());
            CreateIndex("dbo.Graphics", "GraphGroupId");
            AddForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups", "Id");
            DropColumn("dbo.Graphics", "meansTotal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Graphics", "meansTotal", c => c.Double(nullable: false));
            DropForeignKey("dbo.Graphics", "GraphGroupId", "dbo.GraphicGroups");
            DropIndex("dbo.Graphics", new[] { "GraphGroupId" });
            DropColumn("dbo.Graphics", "GraphGroupId");
            DropTable("dbo.GraphicGroups");
        }
    }
}
