﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace proctssor
{
     public class GraphicContext : DbContext
        {
           public GraphicContext()
             : base("DbConnect")
             { }

            public DbSet<GraphicGroup> GraphicGroups { get; set; }
            public DbSet<Graphic> Graphics { get; set; }
            public DbSet<Dot> Dots { get; set; }
        }

     /// <summary>
     /// элемент списка групп графиков, хранящихся в базе
     /// </summary>
     public class GraphicGroup 
     {
         public int Id { get; set; }
         public string Title { get; set; } // название графика формат "от дата время по дата время"
         public double meansTotal { get; set; }//средний показатель температуры
         public virtual ICollection<Graphic> Graphics { get; set; }

         public GraphicGroup()
         {
             Graphics = new List<Graphic>();
         }
     }
    /// <summary>
    /// element of graphics list
    /// </summary>
        public class Graphic
        {
            public int Id { get; set; }
            public string Title { get; set; } // название графика формат "от дата время по дата время"
            public int GraphGroupId { get; set; }//id группы, которому принадлежит график
            public virtual GraphicGroup GraphGroup { get; set; }
            public virtual ICollection<Dot> Dots { get; set; }

            public Graphic()
            {
                Dots = new List<Dot>();
            }
        }

    /// <summary>
    /// GraphicDotsList
    /// </summary>
        public class Dot
        {

            public virtual Graphic Graphic { get; set; }
            public int Id { get; set; }
            public long XTime { get; set; }//время в тактах
            public double YGrade { get; set; }//температура
            public int? GraphId { get; set; }//id графика, которому принадлежит точка
            public virtual Graphic Graph { get; set; }
        }

    
}
