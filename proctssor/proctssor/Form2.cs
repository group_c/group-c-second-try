﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlServerCe;


namespace proctssor
{
    public partial class Form2 : Form
    {
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        
        GraphicContext db = new GraphicContext();
        //List<Form> GList = new List<Form>();
        Form[] arrForm;
        List<GraphicGroup> g = new List<GraphicGroup>();

        #region init
        public Form2()
        {
            InitializeComponent();

            using (var db = new GraphicContext())
            {
                var query = db.GraphicGroups.OrderBy(p => p.Id);

                DataGridViewColumn column = new DataGridViewColumn();
                DataGridViewCell cell = new DataGridViewTextBoxCell();

                dataGridView1.RowCount = query.Count();
                column.DataPropertyName = "meansTotal";
                column.Name = "Среднее значение температуры,°C";
                column.CellTemplate = cell;
                dataGridView1.Columns.Add(column);

                column = new DataGridViewColumn();
                cell = new DataGridViewTextBoxCell();
                //column.DataPropertyName = "meansTotal";
                //column.Name = "Среднее значение температуры";                
                column.CellTemplate = cell;
                dataGridView1.Columns.Add(column);
                dataGridView1.Columns[0].Name = "Графики";
                if(dataGridView1.Columns.Count > 2)
                {
                    int count = dataGridView1.Columns.Count;
                    for (int j = count - 1; j > 1; j--) {
                        dataGridView1.Columns[j].Visible = false;
                    }
                    }


                int i = 0;

                foreach (var item in query)
                {
                    g.Add(item);
                    dataGridView1[0, i].Value = item.Title; 
                    dataGridView1[1, i].Value = item.meansTotal;
                    i++;
                }

                //arrForm = new Form[i];
                int n = dataGridView1.RowCount;
                if (n < 1)
                {
                    MessageBox.Show("Данных в базе нет.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    int count = dataGridView1.Columns.Count;
                    for (int j = 0; j < count; j++)
                    {
                        dataGridView1.Columns[j].Visible = false;
                    }
                }
                arrForm = new Form[n];
            }
        }

        #endregion

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }


        #region click
        /// <summary>
        /// при выделении строки щелчком в списке графиков - показывать окно с графиком
        /// </summary>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           int indx;           
            
           if (dataGridView1.RowCount == 0 || db.GraphicGroups.Count() == 0) { indx = 0; }
           else
           {
               string s = dataGridView1.CurrentRow.Cells[0].Value.ToString();

               //id current group
               indx = dataGridView1.CurrentRow.Index;

               //достаем графики CPU из базы
               bool findForms = false;
               foreach (Form f in Application.OpenForms)
               {
                   if (arrForm[indx] != null && f.Name == arrForm[indx].Name)
                   {                       
                       arrForm[indx].Activate();
                       findForms = true;
                       break;
                   }
               }

               if (!findForms)
               {
                   arrForm[indx] = new FormForGraphs(g[indx]);
                   arrForm[indx].Size = new System.Drawing.Size(900, 350);
                   arrForm[indx].MinimumSize = new System.Drawing.Size(900, 350);
                   arrForm[indx].MaximumSize = new System.Drawing.Size(900, 350);
                   arrForm[indx].Name = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                   arrForm[indx].Show();
               }
           }
        }

        #endregion

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            int i = 0;
            
            
            foreach (Form f in arrForm)
             {
                while (arrForm != null)
                {
                    if (arrForm[i] != null)
                    {
                        arrForm[i].Close();
                        break;
                    }
                    else
                    {
                        break;
                    }
                    } i++;
            }
            
        }

       

    }
}
