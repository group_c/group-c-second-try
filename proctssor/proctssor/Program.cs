﻿using RunOnlyOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace proctssor
{
    public static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (RunOnlyOneClass.ChekRunProgramm("Test")) return;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 f = new Form1();
            Application.Run(f);
        }
    }

}
