﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using proctssor;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void removeGroupGraphics_falseId()
        {
            Form1 form = new Form1();
            string res = form.removeGroupGraphics(-5);
            Assert.AreEqual(res, "Группы графиков с таким Id нет");
        }
        [TestMethod]
        public void removeGroupGraphics_nullId()
        {
            Form1 form = new Form1();
            string res = form.removeGroupGraphics();
            Assert.AreEqual(res, "Группы графиков с таким Id нет");
        }
        [TestMethod]
        public void editGroupGraphics_falseId()
        {
            Form1 form = new Form1();
            string res = form.editGroupGraphics(-5);
            Assert.AreEqual(res, "Группы графиков с таким Id нет");
        }
        [TestMethod]
        public void editGroupGraphics_nullId()
        {
            Form1 form = new Form1();
            string res = form.editGroupGraphics();
            Assert.AreEqual(res, "Группы графиков с таким Id нет");
        }
        [TestMethod]
        public void removeGraphic_falseId()
        {
            Form1 form = new Form1();
            string res = form.removeGraphic(-5);
            Assert.AreEqual(res, "Графика с таким Id нет");
        }
        [TestMethod]
        public void removeGraphic_nullId()
        {
            Form1 form = new Form1();
            string res = form.removeGraphic();
            Assert.AreEqual(res, "Графика с таким Id нет");
        }
        [TestMethod]
        public void removeDot_falseId()
        {
            Form1 form = new Form1();
            string res = form.removeDot(-5);
            Assert.AreEqual(res, "Точки с таким Id нет");
        }
        [TestMethod]
        public void removeDot_nullId()
        {
            Form1 form = new Form1();
            string res = form.removeDot();
            Assert.AreEqual(res, "Точки с таким Id нет");
        }
    }
}
